This folder contains policy specific to Bruce-Grey-Owen Sound Greens.
## art/
Contains some visual artwork such as logos

## bylaws/
The Bylaws are rules governing the internal affairs of the party between general
assemblies.

## constitution/
The Constitution is the main governing document and can only be modified at
General Assemblies, such as the Annual General Meeting, or other general
assemblies where the general membership is invited and achieves quorum. 

## fundraising/
Has documentation related to fundraising

## introductory_template/
Contains documentation for how to introduce oneself and BGOS Green Party for
calls and-or in person meetings.

## plan/
Contains various planning documents

## training/
Has training material which could be valuable for executives and some volunteers

## volunteer/
Contains various documentation for volunteers

## maps/
Contains maps of Bruce Grey-Owen Sound and indigenous land treaties.
