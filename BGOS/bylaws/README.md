# BGOS Bylaws

The Bylaws are a document which governs the internal affairs of party between
General Assemblies. 

In this folder you will also find role specifications as defined in Elections
Canada "Elections Canada Based Role Summaries.docx", and from 
tagdit.com (Proxy_Party_EDA_Handbook.pdf)

roldescription.md has some additional role descriptions made by our board members
