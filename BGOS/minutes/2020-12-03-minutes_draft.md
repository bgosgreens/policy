Meeting of the Executive of the Green Party of Ontario (Bruce Grey - Owen Sound)

December 3, 2020 — 7:00 pm

(virtual meeting)

Present: Corrina Serda \[Communications Chair\]; Jake Bates \[Secretary\]; Andrii Logan Zvorygin \[Organizational Chair\]; Yves Rollin (G.P.C. (B.G. - O.S) President); and Dani Valiquette (G.P.O. (B.G - O.S.) President)

Regrets: Cathay Miller \[Volunteer Chair\]; Theresa Whelan \[Treasurer for both Green Party of Canada and Green Party of Ontario (B.G. - O.S.)); Wayne Shier (Fundraising Chair);

Report from the Green Party of Canada:

It has come to the attention of the Federal wing of the party that there is a high possibility of a general election. Dani pointed out that this will require us to move the Annual General Meeting date up. As well, there is a need to

Yves asked about the forms. Dani has been working on a registration form. It is currently in the form of a Google Form. Corrina said that the functionality of the webpage needs to be updated before we begin to embed forms. This information is

Report from Green Party of Ontario:

Both Dani and Corrina felt it was a very positive experience. It was noted that there are still some procedural problems to address. There appeared to be the potential to vote twice. Diane Sax was announced as the Deputy Leader of the Party.

Dani shared her work on organizing volunteers. She asked that we look this over, and then develop a strategic plan.

Report from the Treasurer:

Dani has not heard from Theresa in a while.

Report from the Organizational Chair:

Logan expressed a concern using Google, as that information is harvested in the United States by a large corporation. It is felt that this information should be stored and used through a Canadian company. He is recommending the program Git Lab. Dani wondered if this means that we will not be using the tools provided by the Parties. In particular, she felt it would be to our detriment to move away from GVote. At the end, it was felt that embedding the forms in the webpage would be best. Yves wondered if Google documents were to move to Git Lab, would the actual document move, or just a link. Also, if things were to be modified, how would that work out. Corrina wondered if we would need to develop an internet. Also, if people can’t figure out a new software, this could be a problem. Corrina is very comfortable with the current platform (Word Press); Logan felt that information could be easier to access.

Dani suggested that Logan hold on to these ideas for the next three to six months, gain some comfort with the current platform, then come back to it then.

Logan also talked about parliamentary procedure. He volunteered to act as the chair for the meeting, following Robert’s Rules. Dani felt it might inhibit discussion; but yields to the consensus of the group.

Report from the Communication Chair:

Corrina is working on a plan for the volunteers.

Report from Fundraising

Dani noted that last year fundraising raised $50,000. We are on target for this year. It was also noted that there is approximately 500 persons who have expressed some interest in the party. In the new year we will host a fundraising for the Green Party of Canada (in preparation for the anticipated federal meeting), as well as an Annual General Meeting.

On December 7 is a planning meeting for 2021. There will be a call party on December 14.

Open Discussion for the Good of the Party:

Logan and Yves wondered about how to sign up someone who has asked to volunteer. Should Cathay be the person to handle this? Or should others?
