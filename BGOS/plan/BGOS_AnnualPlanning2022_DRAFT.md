# Bruce-Grey-Owen Sound Greens 2 Year Election Plan

## Current 2020 Bruce-Grey-Owen Sound Greens

### Executive

| EDA / CA Executive Members          | Bruce-Grey-Owen Sound Greens                               |
|-------------------|------------------------------------------------------------|
| EDA CEO:          |  Yves Rollin                                                 | 
|  EDA Financial Officer Agent: |  Theresa Whelan                                              |
|  CA President:                    |  Danielle Valiquette                                         |
|  CA CFO:                          |  Theresa Whelan                                              |
|  EDA / CA Secretary:              | Jake Bates                      |
|  EDA / CA Fundraising Chair:     | Wayne Shier                     |
|  EDA / CA Organizing Chair:      | Andrii Logan Zvorygin           |
| Volunteer Chair:                |Cathy Miller                    |
| EDA / CA Communications Chair:  |Corrina Serda                   |
| EDA / CA Member-at-Large:       |

## Vision and Mission

|Vision Statements                                                                       |
|----------------------------------------------------------------------------------------|
| As a local BGOS community, we aspire to grow our visibility in the community.          |
| As a local BGOS community, we are committed to making Green an electable option.       |
| As a local BGOS community, we are co-creating a sustainable future for 7+ generations. |

## Planning
### 2020 Scope

|                                                                                                   |                                                                                                                                                                                                                                   |
|---------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| How much money do you currently have?                                                             | Green Party Canada (BGOS) $2000                                                                                                                                                                                                   
                                                                                                                                                                                                                                     
  Green Party Ontario (BGOS) $13000                                                                                                                                                                                                  |
| How much money can you hope to raise?                                                             | Green Party Canada (BGOS) $1500                                                                                                                                                                                                   
                                                                                                                                                                                                                                     
  Green Party Ontario (BGOS) $1500                                                                                                                                                                                                   |
| How many members do you have?                                                                     | Green Party Canada (BGOS) 150                                                                                                                                                                                                     
                                                                                                                                                                                                                                     
  Green Party Ontario (BGOS) 100                                                                                                                                                                                                     |
| How many actively engaged volunteers do you have?                                                 | Approximately 20 very dedicated and 35 dedicated that support both GPC and GPO. When the BGOS Greens host a larger event, we get a high attendance, for example, the AGM and the Town Hall with Mike Shreiner were well attended. |
| How many active, core team members are on your executive - that are responsible for “portfolios”? | CA President, Danielle Valiquette, is the Green Party of Ontario’s Economic Development, Job Creation and Trade shadow critic.                                                                                                    |
|                                                                                                   |

**What skill sets are among your core team?**

-   Administration: Danielle Valiquette, Yves Rollin, Tess Winget, Jake Bates
-   Volunteer outreach / people skills: Tess Winget, Cathy Miller, Andrii Logan Zvorygin
-   Communications - content creation / writing skills: John Butler, Jake Bates, Corrina Serda, Andrii Zvorygin
-   Communications - tech skills / graphics: Danielle Valiquette, Yves Rollin, Corrina Serda, Andrii Zvorygin
-   Event planning: Wayne Shier, Danielle Valiquette, Cathy Miller, Tess Winget
-   Fundraising: Wayne Shier, Cathy Miller, Andrii Logan Zvorygin
-   Data management: Jerry Grant, Danielle Valiquette, Yves Rollin, Tess Winget, Trevor Stokes

<!-- -->

-   -   -   

<span id="anchor-29"></span>2020 Goals

-   -   -   

The following are the BGOS Greens goals for 2020:

-   Recruit multiple candidates for upcoming elections, so that there is a contested candidate race in BGOS for both the next provincial and federal elections.
-   Double the BGOS volunteer base by 2020. Volunteers are shared across both GPC and GPO.
-   Increase the BGOS membership by 30 percent for both GPC and GPO.
-   Improve outreach and communications by increasing traffic to the BGOS website and social media sites.
-   Re-engage Instagram (InProgress) and Twitter (DONE).
-   Begin an Issue Campaign Committee to determine the focus of the issue campaign.
-   Create a Fundraising Committee, for example, to determine if we have one big fundraiser or many little ones. (In Progress)
-   Create an Organizing Committee to review the BGOS Greens constitution, policies, vision/mission, strategic direction and so on.
-   While continuing to build on the existing “Green” communities, BGOS will work to increase engagement in the following communities:

    -   Hanover

    -   Southgate

    -   West Grey

    -   Arran-Elderslie

    -   Peninsula (especially small business)

-   The BGOS Greens would like to greater engage the BGOS Indigenous and youth communities.
-   Encourage BGOS Greens to become more involved with the internal governance of both the GPC and GPO.

<!-- -->

-   -   -   <span id="anchor-30"></span>Long Term Goals

<span id="anchor-31"></span>Detailed Long Term Goals

|                                                                                |                                                                                                                                     |
|--------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------|
| AREA                                                                           | GOALS                                                                                                                               |
| Events                                                                         
                                                                                 
 Outreach Events                                                                 
                                                                                 
 Fundraising Events                                                              
                                                                                 
 AGM                                                                             
                                                                                 
 Team Socials                                                                    |                                                                                                                                     
                                                                                                                                       
  3 annually                                                                                                                           
                                                                                                                                       
  2 annually                                                                                                                           
                                                                                                                                       
  1 annually                                                                                                                           
                                                                                                                                       
  1 (every quarter)                                                                                                                    |
| Fundraising                                                                    | $3,000 2020                                                                                                                         
                                                                                                                                       
  $6,000 2021                                                                                                                          
                                                                                                                                       
  $10,400 2022                                                                                                                         |
| Communications                                                                 
                                                                                 
 Member Outreach                                                                 
                                                                                 
 Volunteer Outreach (by email or phone)                                          
                                                                                 
 Social Media                                                                    
                                                                                 
 GVote: 2 team members learn                                                     |                                                                                                                                     
                                                                                                                                       
  Gvote IDs 1000 by 2021                                                                                                               
                                                                                                                                       
  Gvote IDs 1000 by 2022                                                                                                               
                                                                                                                                       
  Train two new Gvote users by end of year.                                                                                            |
| EDA / CA Growth                                                                
                                                                                 
 Active Volunteers (current: Approximately 20 very dedicated and 35 dedicated )  
                                                                                 
 Regular Donors (current: 5 to 10)                                               |                                                                                                                                     
                                                                                                                                       
  2021 Increase volunteer base by 100                                                                                                  
                                                                                                                                       
  2022 Increase volunteer base by 300                                                                                                  
                                                                                                                                       
  2021 Increase donors to over 30                                                                                                      
                                                                                                                                       
  2022 Increase donors to over 50                                                                                                      |
| Candidate Recruitment                                                          
                                                                                 
 Form committee                                                                  
                                                                                 
 Identify list of prospects (3-5)                                                
                                                                                 
 Outreach meetings with prospects                                                |                                                                                                                                     
                                                                                                                                       
  Form Candidate Recruitment Committee to identify a list of candidate prospects; this will include outreach meetings with prospects.  
                                                                                                                                       
  Potential High Level Timeline                                                                                                        |
| Issues / Community Impact                                                      |                                                                                                                                     |
| Other:                                                                         
                                                                                 
 File EC reports early!                                                          |                                                                                                                                     |

-   -   -   <span id="anchor-32"></span>Costs

|                                                                           |                                          |
|---------------------------------------------------------------------------|------------------------------------------|
| AREA                                                                      | EST. COST/BUDGET                         |
| Events                                                                    
                                                                            
 Outreach Events                                                            
                                                                            
 Fundraising Event                                                          
                                                                            
 AGM                                                                        
                                                                            
 Event Media                                                                
                                                                            
 CA Meetings                                                                |                                          
                                            
  $600 annually                             
                                            
  $500 annually                             
                                            
  $50 annually (venue)                      
                                            
  $300 (promotion budget, ie Facebook ads)  
                                            
  $150 (coffee and cookies)                 |
| Communications                                                            
                                                                            
 Facebook Promotions                                                        |                                          
                                            
  $200 (boosted posts/sales funnels)        |
| **Supplies **(brochures, event materials, donation books, printing, etc.) | <span id="anchor-33"></span>$500         |
| Other / Contingency                                                       | $280 (ten percent of budget)             |
| TOTAL ANNUAL BUDGET:                                                      | **$2630**                                |

<span id="anchor-34"></span>Fundraising

Overall Annual 2022 Goal: $10,000

|                                                                     |                                                                           |
|---------------------------------------------------------------------|---------------------------------------------------------------------------|
| METHOD                                                              | AMOUNT                                                                    |
| Membership Drive (100@10 each)                                      | 2020 Green Party Canada (BGOS) Increase by 30 percent (or 45 memberships) 
                                                                             
  2020 Green Party Ontario (BGOS) Increase by 100 percent                    
                                                                             
  2022 Green Party Canada (BGOS) Double membership                           
                                                                             
  2022 Green Party Ontario (BGOS) Double membership                          |
| 1 Fundraising Event (dinner, music, silent auction, pledge auction) | $3,000                                                                    |
| Passive fundraising at other event (donation jars, “pass the hat”)  | $1,000                                                                    |
| 1- Phone drive                                                      | $3,000                                                                    |
| Email (1 dedicated email series, plus passive donate button)        | $3,000                                                                    |
| Social Media Advertising (Facebook)                                 | $2,000                                                                    |
