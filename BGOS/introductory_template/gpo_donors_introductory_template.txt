Include: Tag: gpo-donor
Announcement Care Calls 2020 Call Script Script

Hi is {Their name} available?

 

Hi {Their name} this is {Your Name}, {Your role at BGOS Greens}. 

 

The BGOS Greens are making care calls to local residents to check in during these uncertain times.

 

Thank you so much for your time!