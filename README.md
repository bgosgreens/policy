This project has public Bruce Grey Owen Sound Green Party related policy documents.

This project exists in accordance with the Global Greens Charter,
The Global Greens Charter is adopted by the GPC Constitution in Article 5.1
and by the GPO Constitution in Article 3 Appendix A.

The Global Greens Charter principle of Participatory Democracy requires:

* individual empowerment through access to all the relevant information required for any decision, and access to education to enable all to participate
* breaking down inequalities of wealth and power that inhibit participation
* building grassroots institutions that enable decisions to be made directly at the appropriate level by those affected, based on systems which encourage civic vitality, voluntary action and community responsibility

And as elucidated in the Global Greens Charter statutes 1.1 "Have as a priority the encouragement and support of grassroots movements and other organisations of civil society working for democratic, transparent and accountable government, at all levels"
and 1.5 "Uphold the right of citizens to have access to official information and to free and independent media"

If you are new to the party or would like a refresher, 
it can be good to read through them, for an understanding of the policy base.

The order of precedent for policy documents is the Global Greens Charter, 
the GPC Constitution, the GPO Constituion, 
the BGOS Constitution, and then the BGOS Bylaws. 

So the policies of any document can be over-ridden by any preceding policy 
document (which has "higher precedence"). So it makes sense to read them from
order of highest precedent to lowest precedent (i.e. start with the Global
Greens Charter). 

A Constitution governs members and is typically modifiable by members at a 
general assembly which reaches quorum.

Bylaws govern the executive(board) and is typically modifiable by the executive 
at an executive meeting which reaches quorum.

If you would like to request an amendment, can contact a relevant board member
for details describing the problem and your proposed solution. 
Thank you. 
